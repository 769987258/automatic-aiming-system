#include <thread>
#include <opencv2/opencv.hpp>
#include <system/AutomaticAimingSystem.h>

int main(int argc, char** argv)
{
    AutomaticAimingSystem  automaticAimingSystem("infantry_1", "/dev/RMUSB");
    automaticAimingSystem.init();

    std::thread producer(&AutomaticAimingSystem::producer, std::ref(automaticAimingSystem));
    std::thread consumer(&AutomaticAimingSystem::consumer, std::ref(automaticAimingSystem));
  //  std::thread port()
    producer.join();
    consumer.join();

    return 0;
}
