/*-----------------------------------------------------------
\@file          Camera.h
\@brief         The camera class
\@author        FYQ
*----------------------------------------------------------*/

#ifndef _DRIVER_CAMERA_H_
#define _DRIVER_CAMERA_H_

#include <common/NonCopyable.h>
#include <driver/CameraSDK/CameraApi.h>
#include <common/Logger.h>
#include <opencv2/opencv.hpp>

enum class CameraParam
{
    LOWFRAMESPEED = 0,
    MIDFRAMESPEED = 1,
    HEIGHTFRAMESPEED = 2,
    ANALOGGIAN = 3,
    EXPOSURETIME = 300, //300
    GAMMA = 40,
    SATURATION = 130
};


class Camera : public NonCopyable, public NonMoveable
{
public:
    /*--------------------------------------------------------------------
    \@brief         camera constructor
    \@param         camera name
    *------------------------------------------------------------------*/
    explicit Camera(std::string cameraName, int imageWidth = 1280, int imageHeight = 1024);

    /*--------------------------------------------------------------------
    \@brief         camera destroyer
    *------------------------------------------------------------------*/
    ~Camera();

    /*--------------------------------------------------------------------
    \@brief
    \@param
    *------------------------------------------------------------------*/
    bool open();

    /*--------------------------------------------------------------------
    \@brief         get image that is opencv format
    \@return        opencv image format
    *------------------------------------------------------------------*/
    cv::Mat  getMat();


private:
    /*--------------------------------------------------------------------
    \@brief         init camera
    \@return        camera init sucess or not
    *------------------------------------------------------------------*/
    bool cameraInit();

    /*--------------------------------------------------------------------
    \@brief         set some camera's param
    \@return        whether setting success or not
    *------------------------------------------------------------------*/
    bool cameraSetting();

    /*--------------------------------------------------------------------
    \@brief         set output image resolution
    \@param         image width
    \@param         image height
    *------------------------------------------------------------------*/
    CameraSdkStatus setCameraResolution(int width, int height);


private:
    std::string                     _cameraName;
    int                             _cameraNums;
    CameraHandle                    _cameraHandle;
    BYTE                           *_rgbImageBuffer;
    int                             _imageWidth;
    int                             _imageHeight;
    tSdkFrameHead                   _frameInfo;
    tSdkCameraCapbility             _cameraCapbility;
};

#endif//!_DRIVER_CAMERA_H_
