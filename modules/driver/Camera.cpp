//
//Creat by FYQ on 2019/8/22
//

#include <driver/Camera.h>

Camera::Camera(std::string cameraName, int imageWidth, int imageHeight) : _cameraName(cameraName),
                                                                          _cameraNums(0),
                                                                          _imageWidth(imageWidth),
                                                                          _imageHeight(imageHeight),
                                                                          _rgbImageBuffer(nullptr)
{
}

Camera::~Camera()
{
    CameraUnInit(_cameraHandle);
}


bool Camera::cameraInit()
{
    _cameraNums = CameraEnumerateDeviceEx(); //return the number of camera

    if(_cameraNums == 0)
    {
        Log_Fatal << "Can't find the camera";
        return false;
    }

    if(CameraInitEx2(const_cast<char*>(_cameraName.c_str()), &_cameraHandle)  != CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "Camera init fail";
        return  false;
    }

    if(CameraGetCapability(_cameraHandle, &_cameraCapbility) != CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "get Camera capability info fail";
        return false;
    }

    Log_Info << "Camera init success";
    return true;

}

CameraSdkStatus Camera::setCameraResolution(int width, int height)
{
    tSdkImageResolution imageResolution = { 0 };

    //set to 0xff for custom resolution
    imageResolution.iIndex = 0xff;

    imageResolution.iWidth = width;
    imageResolution.iWidthFOV = width;

    imageResolution.iHeight = height;
    imageResolution.iHeightFOV = height;

    return CameraSetImageResolution(_cameraHandle, &imageResolution);
}

bool Camera::cameraSetting()
{
    if(setCameraResolution(_imageWidth, _imageHeight) != CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "set resolution fail";
        return false;
    }

    if(CameraSetFrameSpeed(_cameraHandle, static_cast<int>(CameraParam::HEIGHTFRAMESPEED)) != CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "set frame speed fail";
        return false;
    }

    if(CameraSetAeState(_cameraHandle, false) != CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "Auto exporsure close fail";
        return false;
    }

    if(CameraSetExposureTime(_cameraHandle, static_cast<int>(CameraParam::EXPOSURETIME)) != CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "Exporsure time setting fail";
        return false;
    }

    if(CameraSetLutMode(_cameraHandle, LUTMODE_PARAM_GEN) != CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "Lut mode setting fail";
        return false;
    }

    if(CameraSetAnalogGain(_cameraHandle,
                           static_cast<int>(CameraParam::ANALOGGIAN)/_cameraCapbility.sExposeDesc.fAnalogGainStep) !=
                           CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "Gain setting fail";
        return false;
    }

    //the second param still have problem
    if(CameraSetGamma(_cameraHandle, static_cast<int>(CameraParam::GAMMA)) != CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "Gamma setting fail";
        return false;
    }

    if(CameraSetSaturation(_cameraHandle, static_cast<int>(CameraParam::SATURATION)) != CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "Saturation setting fail";
        return false;
    }

    Log_Info << "camera setting success";
    return true;
}

bool Camera::open()
{
    if(!cameraInit())
    {
        Log_Fatal << "Camera init fail";
        return false;
    }

    if(!cameraSetting())
    {
        Log_Fatal << "camera setting fail";
        return false;
    }

    if(CameraSetIspOutFormat(_cameraHandle, CAMERA_MEDIA_TYPE_BGR8) != CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "camera set output format fail";
        return false;
    }

    if(CameraPlay(_cameraHandle) != CAMERA_STATUS_SUCCESS)
    {
        Log_Fatal << "camera play fail";
        return false;
    }

    Log_Info << "camera open success";
    return true;
}

cv::Mat Camera::getMat()
{
    _rgbImageBuffer = CameraGetImageBufferEx(_cameraHandle, &_imageWidth, &_imageHeight, 10);
    if(_rgbImageBuffer == 0)
    {
        return cv::Mat();
    }
    return cv::Mat(cv::Size(_imageWidth, _imageHeight), CV_8UC3, _rgbImageBuffer);
}

