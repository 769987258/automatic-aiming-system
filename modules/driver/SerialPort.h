/*-----------------------------------------------------------
\@file          Port.h
\@brief         The serial port class
\@author        FYQ
*----------------------------------------------------------*/

#ifndef _DRIVER_SERIALPORT_H_
#define _DRIVER_SERIALPORT_H_

#include <sys/stat.h>
#include <sys/types.h>
#include <fcntl.h>
#include <termios.h>
#include <errno.h>
#include <unistd.h>
#include <memory.h>
#include <thread>
#include <mutex>
#include <atomic>

#include <common/Logger.h>

typedef union
{
    float angle;
    uint8_t data[4];
}AngleData;

struct ControlFrame
{
    ControlFrame()
    {
        frameSeq = 0;
        model = 0;
        pitch.angle = 0;
        yaw.angle = 0;
    }

    uint8_t frameSeq;
    uint8_t model;
    AngleData yaw;
    AngleData  pitch;
};

struct FeedBackFrame
{
    uint8_t color;
    uint8_t model;
};

class SerialPort
{
public:
    /*---------------------------------------------------
    \@brief         Constructor
    \@param         port Name
    *---------------------------------------------------*/
    explicit SerialPort(const std::string portName);

    /*---------------------------------------------------
    \@brief         Destructor
    *---------------------------------------------------*/
    ~SerialPort();

public:
    /*---------------------------------------------------
    \@brief         open port.
    \@param         port baudrate.
    *---------------------------------------------------*/
    bool open(speed_t baudRate = B115200);

    /*---------------------------------------------------
    \@brief         close port
    *---------------------------------------------------*/
    void close();

    /*--------------------------------------------------------------------
    \@brief         send data
    \@param         the control frame which need to be send
    \@return        the number of byte that send successfully
    *------------------------------------------------------------------*/
    int send(const ControlFrame& controlFrame);

    /*--------------------------------------------------------------------
    \@brief         recive the serial port data
    \@param         recive success of not
    *------------------------------------------------------------------*/
    bool receive();

    /*--------------------------------------------------------------------
    \@brief         get the feedback data
    \@return        the feedback frame
    *------------------------------------------------------------------*/
    FeedBackFrame getFeedBack();


private:
    /*--------------------------------------------------------------------
    \@brief         parse recived data frame
    \@param         data frame
    *------------------------------------------------------------------*/
    void unpackData(uint8_t* dataBuffer);

    /*--------------------------------------------------------------------
    \@brief         encapsulate the sending data
    \@param
    *------------------------------------------------------------------*/
    void packData(const ControlFrame& controlFrame);

    /*--------------------------------------------------------------------
    \@brief         get the Crc8 check code
    \@param         data buffer
    \@param         data length
    *------------------------------------------------------------------*/
    uint8_t getCrc8CheckSum(uint8_t *dataBuffer, size_t dataLength);

    /*---------------------------------------------------------------------
    \@brief         Verify crc8
    \@param         data buffer
    \@param         data length : data + crc8 (unit: byte)
    *------------------------------------------------------------------*/
    int verifyCrc8CheckSum(uint8_t *dataBuffer, size_t dataLength);

    /*--------------------------------------------------------------------
    \@brief         append the crc8 check code to the data
    \@param         data buffer
    \@param         data length
    *------------------------------------------------------------------*/
    void appendCrc8CheckSum(uint8_t* dataBuffer, size_t dataLength);

    /*--------------------------------------------------------------------
    \@brief         get the crc16 check code
    \@param         data buffer
    \@param         data length : data + crc8 (unit:byte)
    *------------------------------------------------------------------*/
    uint16_t getCrc16CheckSum(uint8_t *dataBuffer,size_t dataLength);

    /*--------------------------------------------------------------------
    \@brief         verify crc16
    \@param         data buffer
    \@param         data length : data + crc16 (unit:byte)
    *------------------------------------------------------------------*/
    uint32_t verifyCrc16CheckSum(uint8_t *dataBuffer, size_t dataLength);

    /*--------------------------------------------------------------------
    \@brief         append the crc16 check code to the data
    \@param         data buffer
    \@param         data length : data + crc16 (unit:byte)
    *------------------------------------------------------------------*/
    void appendCrc16CheckSum(uint8_t *dataBuffer, size_t dataLength);

private:
    std::string                         _portName;
    termios                             _option;
    uint8_t                             *_rxBuffer;
    uint8_t                             *_txBuffer;
    int                                 _fileDescription;
    bool                                _portOpenFlag;
    static const uint8_t                _CRC8_TAB[256];
    static const uint16_t               _CRC16_TAB[256];
    FeedBackFrame                       _feedBackFrame;
};

#endif // !_DIRIVER_SERIALPORT_H_
