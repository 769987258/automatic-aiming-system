/*--------------------------------------------------------------------
\@file          ArmorDetection.h
\@brief         Armor detection and the related classes
\@author        FYQ
*------------------------------------------------------------------*/

#ifndef _PERCEPTION_ARMORDETECTION_H_
#define _PERCEPTION_ARMORDETECTION_H_

#include <opencv2/opencv.hpp>
#include <common/Version.h>
#include <string>
#include <common/Logger.h>

enum Color{
    BLUE = 0,
    GREEN = 1,
    RED =2
};

struct ArmorDetectorParam
{
    int extractColorThreshold = 60;
    int extractBrightnessThreshold = 100;
    float lightAngleThreshold = 25;
    float minLightAspectRatio = 3;
    float lightAngleDiff = 10;
    float minAspectRatio = 1;
    float maxAspectRatio = 7;  //small is 2.5
    float imageCenterX = 640;
    float imageCenterY = 512 + 58;  //512
};

class ArmorLight : public cv::RotatedRect
{
public:
    /*--------------------------------------------------------------------
    \@brief         constructor
    \@param         light bounding rectangle
    \@param         light rotate angle
    *------------------------------------------------------------------*/
    explicit ArmorLight(const cv::RotatedRect& rotatedRect, cv::Point2f *points);
    ~ArmorLight();

public:
    std::vector<cv::Point2f>    rotateRectPoints;
};

class Armor
{
public:
    /*--------------------------------------------------------------------
    \@brief         defult constructor
    *------------------------------------------------------------------*/
    Armor() = default;

    /*--------------------------------------------------------------------
    \@brief         constructor
    \@param         armor image
    \@param         armor center
    \@param         armor bounding rectangle
    \@param         armor angle
    *------------------------------------------------------------------*/
    Armor(cv::Mat& armorImg, cv::RotatedRect& rotatedRect);

    /*--------------------------------------------------------------------
    \@brief         copy constructor
    \@param         copy object
    *------------------------------------------------------------------*/
    Armor(Armor& armor);

    /*--------------------------------------------------------------------
    \@brief         move constructor
    \@param         move object
    *------------------------------------------------------------------*/
    Armor(Armor&& armor) noexcept;

    /*--------------------------------------------------------------------
    \@brief         copy assignment
    \@param         copy object
    *------------------------------------------------------------------*/
    Armor& operator=(Armor& armor);

    /*--------------------------------------------------------------------
    \@brief         move assugbment
    \@param         move object
    *------------------------------------------------------------------*/
    Armor& operator=(Armor&& armor) noexcept;

    /*--------------------------------------------------------------------
    \@brief         deconstructor
    *------------------------------------------------------------------*/
    ~Armor();

public:
    cv::Mat                     armorImg;
    cv::RotatedRect             armorRotatedRect;
};

class ArmorDetector
{
public:
    /*--------------------------------------------------------------------
    \@brief         constructor
    *------------------------------------------------------------------*/
    ArmorDetector() = default;

    /*--------------------------------------------------------------------
    \@brief         destructor
    *------------------------------------------------------------------*/
    ~ArmorDetector() = default;

    /*--------------------------------------------------------------------
    \@brief         detect the armor
    \@param         the input image
    *------------------------------------------------------------------*/
    Armor detect(cv::Mat& srcImg);

    /*--------------------------------------------------------------------
    \@brief         setting the enemy light color
    \@param         enemy light color
    *------------------------------------------------------------------*/
    void setEnemyColor(Color enemyColor);

public:

    /*--------------------------------------------------------------------
    \@brief         setting the area of roi
    \@param         the input image
    *------------------------------------------------------------------*/
    void setRoi(cv::Mat& inputImg);

    /*---------------------------------------------------------------------------------
    \@brief         distill the enemy color region
    \@param         the input image
    \@param[OUT]    the output image which is 8-bit binary image
    \@param         chose detection mode
    *---------------------------------------------------------------------------------*/
    void extractColor(cv::Mat& inputImg, cv::Mat& outputImg,bool detectionMode);

    /*--------------------------------------------------------------------
    \@brief         distill the bightness region
    \@param         the input image
    \@param[OUT]    the output image which is 8-bit binary image
    *------------------------------------------------------------------*/
    void extractBrightness(cv::Mat& inputImg, cv::Mat& outImg);

    /*--------------------------------------------------------------------
    \@brief         find the correct armor lights
    \@param         the extracted color binary image
    \@param         the extracted birght binary image
    \@param[OUT]    the set of armor light which was found
    *------------------------------------------------------------------*/
    bool findArmoredLights(const cv::Mat& binColorImg, const cv::Mat& binBrightImg);

    /*--------------------------------------------------------------------
    \@brief         find the correct armor
    \@param         the src img which use to extract the armor image
    *------------------------------------------------------------------*/
    bool findArmors(cv::Mat& srcImg);

    /*--------------------------------------------------------------------
    \@brief         ajust the rotated rectangle angle to the range of (-90, 90)
    \@param         the rotated rectangle which need to be ajust
    *------------------------------------------------------------------*/
    void ajustAngle(cv::RotatedRect& rect);

    /*--------------------------------------------------------------------
    \@brief         the armor image was transformed into perspective
    \@param         the input image
    \@param[OUT]    the output image
    \@param         the rectangle of armor
    \@param         the left armor light
    \@param         the right armor light
    *------------------------------------------------------------------*/
    void armorPerspectiveTrans(cv::Mat &srcImg, cv::Mat& armorImg, cv::Rect2f& armorRoi, ArmorLight& leftLight, ArmorLight& rightLight);

    /*--------------------------------------------------------------------
    \@brief         make sure the rectangle in the image
    \@param         the  rectangle that need to be jugded
    \@param         the image size
    \@return        safe of not
    *------------------------------------------------------------------*/
    bool makeRectSafe(cv::Rect& rect, cv::Size size);

    /*--------------------------------------------------------------------
    \@brief         use the lights to fitting the bouding rotated rectangle
    \@param         left light
    \@param         right light
    \return         the fitting rotated rectangle
    *------------------------------------------------------------------*/
    cv::RotatedRect boundingRotatedRect(const cv::RotatedRect &left, const cv::RotatedRect &right);

    /*--------------------------------------------------------------------
    \@brief         make the decision of striking
    \@return        the armor that decide to attack
    *------------------------------------------------------------------*/
    Armor strikingDecision();

public:
    Color                           _enemyColor;
    std::vector<ArmorLight>         _armorLights;
    std::vector<Armor>              _armors;
    cv::Rect                        _detecArea;
    cv::RotatedRect                 _lastRotateRect;
    ArmorDetectorParam              _detectParam;
    int                             _lostCount;
};

#endif //!_PERCEPTION_ARMORDETECTION_H_