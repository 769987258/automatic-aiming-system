//
// Create by FYQ on 2020/1/3
//

#include <perception/RuneDetection.h>

RuneLeaf::RuneLeaf(RuneLeaf &runeLeaf)
{
    *this = runeLeaf;
}

RuneLeaf::RuneLeaf(RuneLeaf &&runeLeaf) noexcept
{
    *this = std::move(runeLeaf);
}

RuneLeaf& RuneLeaf::operator=(RuneLeaf &runeLeaf)
{
    this->_shotPoint = runeLeaf._shotPoint;
    this->_aimArmor = runeLeaf._aimArmor;
    return *this;
}

RuneLeaf& RuneLeaf::operator=(RuneLeaf &&runeLeaf) noexcept
{
    this->_aimArmor = std::move(runeLeaf._aimArmor);
    this->_shotPoint = std::move(runeLeaf._shotPoint);
    return *this;
}

void RuneDector::setRuneColor(Color enemyColor)
{
    if(enemyColor == BLUE)
    {
        _runeColor = RED;
    }
    else
    {
        _runeColor = BLUE;
    }
}

void RuneDector::extractColor(cv::Mat &inputImg, cv::Mat &outputImg)
{
    static std::vector<cv::Mat> bgr;
    cv::split(inputImg, bgr);
    switch (_runeColor)
    {

        case (Color::RED) : ////red color
        {
            outputImg = bgr[Color::RED] - bgr[Color::BLUE];
            break;
        }
        case (Color::BLUE) : ////blue color
        {
            outputImg = bgr[Color::BLUE] - bgr[Color::RED];
            break;
        }
    }
    cv::threshold(outputImg, outputImg, _detectorParam.extractColorThreshold, 255, cv::THRESH_BINARY);
    bgr.clear();
}

bool RuneDector::findRuneLeaf(const cv::Mat &binColorImg, RuneLeaf &leaf)
{
    static std::vector<std::vector<cv::Point>> contours;
    static std::vector<cv::Vec4i> hierarchy;
    cv::findContours(binColorImg, contours, hierarchy, cv::RETR_CCOMP, cv::CHAIN_APPROX_NONE);

    for(size_t i = 0; i < contours.size(); i++)
    {
        int sonContourSum = 0;
        double leafContourArea = cv::contourArea(contours[i]);
        if (leafContourArea > _detectorParam.minLeafArea && leafContourArea < _detectorParam.maxLeafArea)
        {
            if (hierarchy[i][2] != -1)
            {
                int j = i;
                while (hierarchy[j][2] != -1)
                {
                    sonContourSum++;
                    j = hierarchy[j][2];
                    if (sonContourSum > 1)
                    {
                        break;
                    }
                }
                if (sonContourSum == 1)
                {
                    cv::RotatedRect leafRect = cv::fitEllipse(contours[i]);
                    double leafRectArea = leafRect.size.width * leafRect.size.height;
                    double areaRatio = leafContourArea / leafRectArea;
                    if (areaRatio > _detectorParam.minAreaRatio && areaRatio < _detectorParam.maxAreaRatio)
                    {
                        if (contours[hierarchy[i][2]].size() >= 5)
                        {
                                cv::RotatedRect aimArmor = cv::fitEllipse(contours[hierarchy[i][2]]);
                                cv::Point2f shotPoint = aimArmor.center;
                                leaf._shotPoint = shotPoint;
                                leaf._aimArmor = aimArmor;
                                leaf._leafContourArea = leafContourArea;
                                leaf._leafRectArea = leafRectArea;
                                contours.clear();
                                hierarchy.clear();
                                return true;
                        }
                    }
                }
            }
        }
    }

    contours.clear();
    hierarchy.clear();
    return false;
}

bool RuneDector::isSwitch(RuneLeaf leaf)
{
    if(std::abs(leaf._shotPoint.x - _lastLeafPoint.x) > 30 ||
       std::abs(leaf._shotPoint.y - _lastLeafPoint.y) > 30)
    {
        return true;
    }
    return false;
}

void RuneDector::fittingCircle(std::vector<cv::Point2f> &pointSet, double &centerX, double &centerY, double &radius)
{
    int N = pointSet.size();
    double sumX = 0.0f, sumY = 0.0f;
    double sumX2 = 0.0f, sumY2 = 0.0f;
    double sumX3 = 0.0f, sumY3 = 0.0f;
    double sumXY = 0.0f, sumX1Y2 = 0.0f, sumX2Y1 = 0.0f;

    for(size_t i = 0; i < N; i++)
    {
        double x = pointSet[i].x;
        double y = pointSet[i].y;
        double x2 = x * x;
        double y2 = y * y;
        double x3 = x2 * x;
        double y3 = y2 *y;

        sumX += x;
        sumY += y;
        sumX2 += x2;
        sumY2 += y2;
        sumX3 += x3;
        sumY3 += y3;
        sumXY += x * y;
        sumX1Y2 += x * y2;
        sumX2Y1 += x2 * y;
    }

    double C = N * sumX2 - sumX * sumX;
    double D = N * sumXY - sumX * sumY;
    double E = N * sumX3 + N * sumX1Y2 - (sumX2 + sumY2) * sumX;
    double G = N * sumY2 - sumY * sumY;
    double H = N * sumX2Y1 + N * sumY3 - (sumX2 + sumY2) * sumY;
    double a = (H * D - E * G) / (C * G - D * D);
    double b = (H * C - E * D) / (D * D - G * C);
    double c = -(a * sumX + b * sumY + sumX2 + sumY2) / N;

    centerX = a / (-2);
    centerY = b / (-2);
    radius = std::sqrt(a*a + b*b - 4*c) / 2;

    pointSet.clear();
}

