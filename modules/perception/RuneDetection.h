/*-------------------------------------------------------------------
\@file          RuneDetection.h
\@brief         Rune detection and the related classes
\@author        FYQ
*------------------------------------------------------------------*/
#ifndef _PERCEPTION_RUNEDETECTION_H_
#define _PERCEPTION_RUNEDETECTION_H_

#include <opencv2/opencv.hpp>
#include <perception/ArmorDetection.h>
#include <common/Version.h>
#include <common/Logger.h>

struct RuneDetectorParam
{
    int extractColorThreshold = 60;
    double minLeafArea = 5500.0;
    double maxLeafArea = 6200.0;
    double minAreaRatio = 0.48;
    double maxAreaRatio = 0.55;
};

class RuneLeaf
{
public:
    RuneLeaf() = default;
    ~RuneLeaf() = default;

    RuneLeaf(RuneLeaf& runeLeaf);
    RuneLeaf(RuneLeaf&& runeLeaf) noexcept;
    RuneLeaf& operator=(RuneLeaf& runeLeaf);
    RuneLeaf& operator=(RuneLeaf&& runeLeaf) noexcept;

public:
    cv::RotatedRect                 _aimArmor;
    cv::Point2f                     _shotPoint;
    double                          _leafContourArea;
    double                          _leafRectArea;
};

class RuneDector
{
public:
    RuneDector() = default;
    ~RuneDector() = default;
    void setRuneColor(Color enemyColor);
    void extractColor(cv::Mat& inputImg, cv::Mat& outputImg);
    bool findRuneLeaf(const cv::Mat& binColorImg, RuneLeaf &leaf);
    bool isSwitch(RuneLeaf leaf);
    void fittingCircle(std::vector<cv::Point2f> &pointSet, double &centerX, double &centerY, double &radius);

public:
    cv::Point2f                     _lastLeafPoint;
    Color                           _runeColor;
    RuneDetectorParam               _detectorParam;
};



#endif //!_PERCEPTION_RUNEDETECTION_H_