/*---------------------------------------------------------------------------------
\@file				NonCopyable.h
\@brief				Inherit this object can save time to accomplish the
					default copy constructor and copy assign operator,
					and accomplish the object semantics.
\@author			zhao
*---------------------------------------------------------------------------------*/

#ifndef _COMMON_NON_COPYABLE_H_
#define _COMMON_NON_COPYABLE_H_

namespace _non_copyable
{
	class NonCopyable
	{
	protected:
		NonCopyable() = default;
		virtual ~NonCopyable() = default;
	private:
		NonCopyable(const NonCopyable&) = delete;
		const NonCopyable& operator=(const NonCopyable&) = delete;
	};
} // _non_copyable

namespace _non_moveable
{
	class NonMoveable
	{
	protected:
		NonMoveable() = default;
		virtual ~NonMoveable() = default;
	private:
		NonMoveable(NonMoveable&&) = delete;
		const NonMoveable& operator=(NonMoveable&&) = delete;
	};
} // _non_moveable

namespace _non_constructible
{
	class NonConstructible
	{
	private:
		NonConstructible() = default;
	protected:
		virtual ~NonConstructible();
	};
} // _non_constructiable

typedef _non_copyable::NonCopyable NonCopyable;
typedef _non_moveable::NonMoveable NonMoveable;
typedef _non_constructible::NonConstructible NonConstructible;

#endif // !_UTIL_NON_COPYABLE_H_
