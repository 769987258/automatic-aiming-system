/**-----------------------------------------------------------------------
\@file				Logger.h
\@brief				The lightweight Logger
\@author			zhao
*-----------------------------------------------------------------------*/
#ifndef _COMMON_LOGGER_H_
#define _COMMON_LOGGER_H_

#include <common/NonCopyable.h>
#include <common/Version.h>
#include <sys/stat.h>
#include <assert.h>
#include <zconf.h>
#include <stdio.h>
#include <time.h>
#include <sstream>
#include <fstream>
#include <thread>
#include <mutex>
#include <map>

#define __Log(level)  logging::Log(level, __FILE__, __FUNCTION__, __LINE__)
#define __FLog(level) logging::FLog(level, __FILE__, __FUNCTION__, __LINE__)


#define Log_Fatal	  __Log("Fatal")
#define Log_Warning	  __Log("Warning")
#define FLog_Fatal    __FLog("Fatal")
#define FLog_Warning  __FLog("Warning")

#ifdef DEBUG
#define Log_Debug	  __Log("Debug")
#define Log_Info	  __Log("Info")
#define FLog_Debug    __FLog("Debug")
#define FLog_Info     __FLog("Info")
#else
#define Log_Debug	  0 &&__Log("Debug")
#define Log_Info	  0 &&__Log("Info")
#define FLog_Debug    0 &&__FLog("Debug")
#define FLog_Info     0 &&__FLog("Info")
#endif

namespace logging
{
	class LogStream;
	class BaseLogger;
	class ConsoleLogger;
	class FileLogger;

	/*----------------------------------------------------------
	\@brief			The logger's level
	*----------------------------------------------------------*/
	enum class LOG_LEVEL
	{
		LOG_LEVEL_FATAL = 1,

		LOG_LEVEL_WARNING = 2,

		LOG_LEVEL_DEBUG = 3,

		LOG_LEVEL_INFO = 4,
	};

	/*----------------------------------------------------------
	\@brief			The input logger level string map
	*----------------------------------------------------------*/
	static const std::map<std::string, LOG_LEVEL> LOG_LEVEL_STRING
	{
		{"Fatal", LOG_LEVEL::LOG_LEVEL_FATAL },

		{"Warning", LOG_LEVEL::LOG_LEVEL_WARNING },

		{"Debug", LOG_LEVEL::LOG_LEVEL_DEBUG },

		{"Info", LOG_LEVEL::LOG_LEVEL_INFO },
	};

	/*----------------------------------------------------------
	\@brief			The console color map
	*----------------------------------------------------------*/
	static const std::map<LOG_LEVEL, std::string> CONSOLE_COLOR
	{
        {LOG_LEVEL::LOG_LEVEL_FATAL, "\e[0;31m"},

        {LOG_LEVEL::LOG_LEVEL_WARNING, "\e[1;33m"},

        {LOG_LEVEL::LOG_LEVEL_DEBUG, "\e[0;32m"},

        {LOG_LEVEL::LOG_LEVEL_INFO, "\e[1;37m"},
	};

	/*----------------------------------------------------------
	\@brief			The log stream
	*----------------------------------------------------------*/
	class LogStream : public std::ostringstream
	{
	public:
		/*-------------------------------------------------------
		\@brief		Constructor
		*-------------------------------------------------------*/
		LogStream(std::string logLevelString,
				  BaseLogger& baseLogger,
				  const char* file,
				  const char* function,
				  size_t line);

		/*-------------------------------------------------------
		\@brief		Copy constructor
		*-------------------------------------------------------*/	
		LogStream(const LogStream& logStream);

		/*-------------------------------------------------------
		\@brief		Destructor
		*-------------------------------------------------------*/
		~LogStream();

	private:
		BaseLogger&			_base_logger;

		std::string			_log_level_string;

		const char* 		_file;

		const char*			_function;

		size_t				_line;
	};

	/*------------------------------------------------------------
	\@brief			The base logger as the loggers' interface
	\@note			The logger inherit the NonCopyable,so it 
					as a singleton which can't be copied and moved
	*------------------------------------------------------------*/
	class BaseLogger : public NonCopyable, public NonMoveable
	{
		friend class LogStream;

		using NonCopyable::NonCopyable;

		using NonMoveable::NonMoveable;
	public:
		/*-------------------------------------------------------
		\@brief		To get a LogStream object
		\@param		Set the log level
		\@param 	The file name calling this function
		\@param		The function name calling this function
		\@param 	The line number calling this function		
		\@return	A LogStream object
		\@see		LOG_LEVEL_STRING
		*-------------------------------------------------------*/
		virtual LogStream operator()(std::string logLevel,
									 const char* file,
									 const char* function,
									 size_t line);
	protected:
		/*-------------------------------------------------------
		\@brief		Get local Time
		\@return	The string records local time
		*-------------------------------------------------------*/
		static std::string get_local_time();

		/*-------------------------------------------------------
		\@brief		The logger printed stream interface
		*-------------------------------------------------------*/
		virtual void log(const char* message,
						 const char* file,
						 const char* function,
						 unsigned int line,
						 std::string level) = 0;
	};

	/*----------------------------------------------------------
	\@brief			Print the message to the console
	*----------------------------------------------------------*/
	class ConsoleLogger : public BaseLogger
	{
		using BaseLogger::BaseLogger;
	public:
		/*-------------------------------------------------------
 		\@brief	    Get the console logger instance
		\@return	ConsoleLogger instance
 		*------------------------------------------------------*/
		static ConsoleLogger& getConsoleLogger();

	protected:
		/*----------------------------------------------------------
		\@brief			Print the message and locate calling function
						to console
		\@param			Input message
		\@param 		The file name calling this function
		\@param			The function name calling this function
		\@param 		The line number calling this function
		\@param			Logger level	
		*----------------------------------------------------------*/
		virtual void log(const char* message,
						 const char* file,
						 const char* function,
						 unsigned int line,
						 std::string level) override;

	private:
	    /*----------------------------------------------------------
	    \@brief        Default constructor
	    *---------------------------------------------------------*/
	    explicit ConsoleLogger() = default;

	private:
		std::mutex 		 _mutex;
	};

	/*----------------------------------------------------------
	\@brief			Print the message to the log file
	*----------------------------------------------------------*/
	class FileLogger : public BaseLogger
	{
	public:
		/*-------------------------------------------------------
		\@brief		Get the FileLogger instance
		\@return	FileLogger instance
		*-------------------------------------------------------*/
		static FileLogger& getFileLogger();

		/*-------------------------------------------------------
		\@brief		Destructor
		\@note		Only the FileLogger is dead, then it will flush 
					the buffer and print to the log file.
		*-------------------------------------------------------*/
		~FileLogger() final;

	protected:
		/*----------------------------------------------------------
		\@brief			Print the message and locate calling function
						to log file
		\@param			Input message
		\@param 		The file name calling this function
		\@param			The function name calling this function
		\@param 		The line number calling this function
		\@param			Logger level	
		*----------------------------------------------------------*/
		virtual void log(const char* message,
						 const char* file,
						 const char* function,
						 unsigned int line,
						 std::string level) override;
	private:
        /*----------------------------------------------------------
        \@brief        Default constructor
        *---------------------------------------------------------*/
        explicit FileLogger();

	private:
		std::ofstream		_ofstream;

		std::mutex			_mutex;

		std::string         _file;
	};

    #define Log ConsoleLogger::getConsoleLogger()
    #define FLog FileLogger::getFileLogger()

} // namespace logging

#endif // !_UTIL_LOGGER_H_

