//
// Created by robotzhao on 10/25/18.
//

#include <common/Logger.h>

using namespace logging;

/*----------------------------------------------------------------------------------
\@brief     Constructor
*----------------------------------------------------------------------------------*/
LogStream::LogStream(std::string logLevelString,
                     BaseLogger& baseLogger,
                     const char* file,
                     const char* function,
                     size_t line): _log_level_string(std::move(logLevelString)),
                                   _base_logger(baseLogger)
{
    _file = file;
    _function = function;
    _line = line;
}


/*----------------------------------------------------------------------------------
\@brief		Copy constructor
*----------------------------------------------------------------------------------*/
LogStream::LogStream(const LogStream& logStream): _log_level_string(logStream._log_level_string),
                                                  _base_logger(logStream._base_logger)
{
    _file = logStream._file;
    _function = logStream._function;
    _line = logStream._line;
}


/*----------------------------------------------------------------------------------
\@brief		Destructor
\@note      While the log stream call the destructor, it will log the message.
*----------------------------------------------------------------------------------*/
LogStream::~LogStream()
{
    _base_logger.log(this->str().c_str(),
                     _file,
                     _function,
                     _line,
                     _log_level_string);
}

/*----------------------------------------------------------------------------------
\@brief		To get a LogStream object
\@param		Set the log level
\@param 	The file name calling this function
\@param		The function name calling this function
\@param 	The line number calling this function
\@return	A LogStream object
\@see		LOG_LEVEL_STRING
*-----------------------------------------------------------------------------------*/
LogStream BaseLogger::operator()(std::string logLevel,
                                 const char* file,
                                 const char* function,
                                 size_t line)
{
    auto iterator = LOG_LEVEL_STRING.find(logLevel);
    assert(iterator != LOG_LEVEL_STRING.end());
    return LogStream(logLevel,
                     *this,
                     file,
                     function,
                     line);
}

/*-----------------------------------------------------------------------------------
\@brief		Get local Time
\@return	The string records local time
*-----------------------------------------------------------------------------------*/
std::string BaseLogger::get_local_time()
{
    time_t currentTime;
    struct tm* timePointer;
    time(&currentTime);
    timePointer = localtime(&currentTime);

    return std::to_string(timePointer->tm_year + 1900) + "-" +
           std::to_string(timePointer->tm_mon + 1) + "-" +
           std::to_string(timePointer->tm_mday) + " " +
           std::to_string(timePointer->tm_hour) + ":" +
           std::to_string(timePointer->tm_min) + ":" +
           std::to_string(timePointer->tm_sec);
}

/*-----------------------------------------------------------------------------------
 \@brief       Get the ConsoleLogger instance
 \@return      ConsoleLogger instance
 *---------------------------------------------------------------------------------*/
ConsoleLogger& ConsoleLogger::getConsoleLogger()
{
    static ConsoleLogger consoleLogger;
    return consoleLogger;
}

/*-----------------------------------------------------------------------------------
\@brief			Print the message and locate calling function to console
\@param			Input message
\@param 		The file name calling this function
\@param			The function name calling this function
\@param 		The line number calling this function
\@param			Logger level
*-----------------------------------------------------------------------------------*/
void ConsoleLogger::log(const char* message,
                        const char* file,
                        const char* function,
                        unsigned int line,
                        std::string level)
{
    std::lock_guard<std::mutex> lock(_mutex);
    std::string time = std::move(get_local_time());
    std::string color = CONSOLE_COLOR.find(LOG_LEVEL_STRING.find(level)->second)->second;
    
    if(level == "Info")
    {
        //only print the message to console
        printf("%s[%s][%s][message]:%s\n\e[0m", color.c_str(),
                                                time.c_str(),
                                                level.c_str(),
                                                message);
    }
    else
    {
        printf("%s[%s][%s][message]:%s [file]:%s [function]:%s [line]:%u\n\e[0m", color.c_str(),
                                                                                  time.c_str(),
                                                                                  level.c_str(),
                                                                                  message,
                                                                                  file,
                                                                                  function,
                                                                                  line);
    }
}

/*-----------------------------------------------------------------------------------
 \@brief       Get the ConsoleLogger instance
 \@return      ConsoleLogger instance
 *---------------------------------------------------------------------------------*/
FileLogger& FileLogger::getFileLogger()
{
    static FileLogger fileLogger;
    return fileLogger;
}

/*----------------------------------------------------------------------------------
\@brief         Constructor
 *----------------------------------------------------------------------------------*/
FileLogger::FileLogger() : _file("../log/" + get_local_time() + ".log")
{
    mkdir("../log", S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    this->_ofstream.open(_file.c_str(),
                         std::ios_base::out |
                         std::ios_base::app |
                         std::ios_base::ate);
}

/*----------------------------------------------------------------------------------
\@brief        Destructor
*----------------------------------------------------------------------------------*/
FileLogger::~FileLogger()
{
    this->_ofstream.flush();
    this->_ofstream.close();
}

/*----------------------------------------------------------------------------------
\@brief			Print the message and locate calling function to log file
\@param			Input message
\@param 		The file name calling this function
\@param			The function name calling this function
\@param 		The line number calling this function
\@param			Logger level
*----------------------------------------------------------------------------------*/
void FileLogger::log(const char* message,
                     const char* file,
                     const char* function,
                     unsigned int line,
                     std::string level)
{
    std::lock_guard<std::mutex> lock(_mutex);
    std::string time = std::move(get_local_time());

    if(level == "Info")
    {
        _ofstream << "[" << time << "]" 
                  << "[" << level << "]"
                  << "[" << "message" << "]:"
                  << message << "\n";
    }
    else
    {
        _ofstream << "[" << time << "]" 
                  << "[" << level << "]"
                  << "[" << "message" << "]:"
                  << message << " "
                  << "[" << "file" << "]:"
                  << file << " "
                  << "[" << "function" << "]:"
                  << function << " "
                  << "[" << "line" << "]:"
                  << line << "\n";
    }
 
}
