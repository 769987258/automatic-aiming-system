/*-----------------------------------------------------------
\@file          CycleQueue.h
\@brief         The thread safe Cycle queue
\@author        FYQ
*----------------------------------------------------------*/

#ifndef _COMMON_CYCLEQUEUE_H_
#define _COMMON_CYCLEQUEUE_H_

#include <mutex>
#include <thread>
#include <vector>
#include <condition_variable>


template <typename Type>
class CycleQueue
{
public:
    CycleQueue(std::size_t capacity) : _capacity(capacity),
                                       _head(-1),
                                       _tail(-1),
                                       _size(0)
    {
        _queue.resize(_capacity + 1);
    }

    ~CycleQueue()
    {
    }

    void push(const Type& object)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _cv.wait(lock, [&]()->bool
        {
           return _size != _capacity;
        });
        _tail = (_tail + 1) % (_capacity + 1);
        _queue[_tail] = object;
        ++_size;
//        Log_Info << "tail: " << _tail;
//        Log_Info<< "size :" << _size;
        _cv.notify_one();
    }

    void pop(Type& object)
    {
        std::unique_lock<std::mutex> lock(_mutex);
        _cv.wait(lock,[&]()-> bool
        {
           return _size != 0;
        });
        _head = (_head + 1) % (_capacity + 1);
        object = _queue[_head];
        --_size;
    //    Log_Info << "head: " << _head;
        _cv.notify_one();
    }

    bool empty()
    {
        return _size == 0;
    }

private:
    std::size_t                    _capacity;
    int                            _size;
    int                            _head;
    int                            _tail;
    std::condition_variable        _cv;
    std::mutex                     _mutex;
    std::vector<Type>              _queue;
};

#endif //!_COMMON_CYCLEQUEUE_H_