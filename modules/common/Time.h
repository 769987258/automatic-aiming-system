/*-----------------------------------------------------------------------
\@file				Time.h
\@brief				get systime
\@author			fyq
*-----------------------------------------------------------------------*/
#ifndef _COMMON_TIME_H_
#define _COMMON_TIME_H_

#include <sys/time.h>

static double getsystime(){
    timeval tv;
    gettimeofday(&tv, nullptr);
    return tv.tv_usec / 1000.0 + tv.tv_sec * 1000.0;
}

class Timer {
public:
    Timer() = default;
    ~Timer() = default;
    void getSystime(double &t);
    double getTimeIntervalms(const double &now, const double &last);
};


#endif //!_COMMON_TIME_H_
