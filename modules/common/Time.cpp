//
// Created FYQ on 19-12-18.
//

#include <common/Time.h>


void Timer::getSystime(double &t)
{
    static double time_base = getsystime();
    timeval tv;
    gettimeofday(&tv, nullptr);
    t = tv.tv_usec / 1000.0 + tv.tv_sec * 1000.0 - time_base;
}

double Timer::getTimeIntervalms(const double &now, const double &last)
{
     return now - last;
}

