//
//Creat by FYQ on 2019/12/2
//

#include <common/SolveAngle.h>


double SolveAngler::getDistance(float imgH)
{
    double imagingPlaneHeigh = imgH * MVCAMERA_HEIGHT_PIX;
    return LENS_FOCAL * ARMOR_HEIGHT/imagingPlaneHeigh;
}

void SolveAngler::getAngle(float x, float y, float &yaw, float &pitch)
{
    float positionX = (x - MVCAMERA_CX) /MVCAMERA_FX;
    float positionY = (y - MVCAMERA_CY) /MVCAMERA_FY;

    yaw = static_cast<float>(atan(positionX)*57.2957805f);
    pitch = static_cast<float>(atan(positionY)*57.2957805f);

}