/*-----------------------------------------------------------
\@file          SolveAngle.h
\@brief         solve the angle of armor
\@author        FYQ
*----------------------------------------------------------*/

#ifndef _COMMON_SOLVEANGLE_H_
#define _COMMON_SOLVEANGLE_H_

#include <opencv2/opencv.hpp>
#include <math.h>
#include <perception/ArmorDetection.h>

#define MVCAMERA_FX 1800.7
#define MVCAMERA_FY 1814.4
#define MVCAMERA_CX 640
#define MVCAMERA_CY 512     //494
#define MVCAMERA_WIDTH_PIX 0.005 //mm
#define MVCAMERA_HEIGHT_PIX 0.00468
#define ARMOR_HEIGHT 58  //57
#define LENS_FOCAL 8

class SolveAngler
{
public:
    SolveAngler() = default;
    ~SolveAngler() = default;
    double getDistance(float imgH);
    void getAngle(float x, float y, float &yaw, float &pitch);
};

#endif //!_COMMON_SOLVEANGLE_H_