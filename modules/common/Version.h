/*-----------------------------------------------------------
\@file          Version.h
\@brief         select the compiled version
\@author        FYQ
*----------------------------------------------------------*/

#include <opencv2/opencv.hpp>


#define DEBUG
//#define RELEASE

#ifdef DEBUG
#define IMSHOW(name, img)  (cv::imshow(name,img))
#define WAITKEY(time)      (cv::waitKey(time))
#endif

#ifndef DEBUG
#define IMSHOW(name, img)
#define WAITKEY(time)
#endif
