/*--------------------------------------------------------------------
\@file          AutomaticAimingSystem.h
\@brief         the system abstract class
\@author        FYQ
*------------------------------------------------------------------*/

#ifndef _SYSTEM_AUTOMATICAIMINGSYSTEM_H_
#define _SYSTEM_AUTOMATICAIMINGSYSTEM_H_

#include <common/Logger.h>
#include <common/CycleQueue.h>
#include <driver/SerialPort.h>
#include <driver/Camera.h>
#include <perception/ArmorDetection.h>
#include <common/Time.h>
#include <common/SolveAngle.h>
#include <signal.h>

class AutomaticAimingSystem
{
public:
    /*--------------------------------------------------------------------
    \@brief         constructor
    \@param         the camera name
    \@param         the port name
    *------------------------------------------------------------------*/
    AutomaticAimingSystem(std::string cameraName, std::string portName);

    /*--------------------------------------------------------------------
    \@brief         destructror
    *------------------------------------------------------------------*/
    ~AutomaticAimingSystem();

    /*--------------------------------------------------------------------
    \@brief     automatic aiming system all modules initialized
    *------------------------------------------------------------------*/
    bool init();

    /*--------------------------------------------------------------------
    \@brief     image acquisition
    *------------------------------------------------------------------*/
    void producer();

    /*--------------------------------------------------------------------
    \@brief     image processing
    *------------------------------------------------------------------*/
    void consumer();

    /*--------------------------------------------------------------------
    \@brief     receive the serial message
    *------------------------------------------------------------------*/
    void serialReceiver();

private:
    /*--------------------------------------------------------------------
    \@brief     quit signal handle
    *------------------------------------------------------------------*/
    static void signalHandler(int);

    /*--------------------------------------------------------------------
    \@brief     init the quit signal handle
    *------------------------------------------------------------------*/
    void initSignal();

private:
    SerialPort                           _serial;
    Camera                               _camera;
    CycleQueue<cv::Mat>                  _imageBuffer;
    Armor                                _aimArmor;
    ArmorDetector                        _detector;
    Color                                _enemyColor;
    FeedBackFrame                        _feedbackData;
    bool                                 _serialOpenFlag;
    Timer                                _recorder;
    SolveAngler                          _soloveAngler;
    cv::KalmanFilter                     _kalMan;
    static bool                          _quitFlag;

};


#endif //!_SYSTEM_AUTOMATICAIMINGSYSTEM_H_