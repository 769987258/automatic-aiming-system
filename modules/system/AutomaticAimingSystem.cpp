//
// Create by FYQ on 2019/8/26
//

#include <system/AutomaticAimingSystem.h>
#include <common/SolveAngle.h>
#include <bitset>

bool AutomaticAimingSystem::_quitFlag;

/*--------------------------------------------------------------------
\@brief         Constructor
\@param         the camera name
\@param         the port name
*------------------------------------------------------------------*/
AutomaticAimingSystem::AutomaticAimingSystem(std::string cameraName, std::string portName) :_camera(cameraName),
                                                                                            _serial(portName),
                                                                                            _imageBuffer(1),
                                                                                            _serialOpenFlag(false)
{
    _feedbackData.model = 0;
    _feedbackData.color = 0;

    _kalMan.init(4, 2, 0);
    _kalMan.transitionMatrix = (cv::Mat_<float>(4,4) <<
                                1,0,1,0,
                                0,1,0,1,
                                0,0,1,0,
                                0,0,0,1);
    cv::setIdentity(_kalMan.measurementMatrix);
    cv::setIdentity(_kalMan.processNoiseCov, cv::Scalar::all(1e-10));
    cv::setIdentity(_kalMan.measurementNoiseCov, cv::Scalar::all(1e-1));
    cv::setIdentity(_kalMan.errorCovPost, cv::Scalar::all(1));

}
/*--------------------------------------------------------------------
\@brief         destructor
*------------------------------------------------------------------*/
AutomaticAimingSystem::~AutomaticAimingSystem()
{
}

/*--------------------------------------------------------------------
\@brief         atuotmatic aiming system init
\@return        init success or not
*------------------------------------------------------------------*/
bool AutomaticAimingSystem::init()
{
    //initSignal();
    //Todo： 初始化方式仍需改进，如何进行相机和串口的循环自检与重连。初始化是也应接受下位机发上来的相关消息，进行视觉程序的初始化
    if(!_camera.open() || !_serial.open())
    {
        return false;
    }
    _serialOpenFlag = true;
    _detector.setEnemyColor(BLUE);
    return true;
}

/*--------------------------------------------------------------------
\@brief         the image capture
*------------------------------------------------------------------*/
void AutomaticAimingSystem::producer()
{
    for(;;)
    {
        //Todo：待大部分功能完成后进行队列效率的优化，思考缓冲区为何会对下位机造成影响。
        if(_quitFlag)
        {
            return;
        }
        _imageBuffer.push(_camera.getMat().clone());     //若不用clone，则会进行浅拷贝，导致前后两幅指针相同的图像赋值时，后一幅图像无效。
    }
}

/*--------------------------------------------------------------------
\@brief         the image processing
*------------------------------------------------------------------*/
void AutomaticAimingSystem::consumer()
{
    for(;;)
    {
        //ToDo：代码整体架构需要可能需要重新架构，单元测试仍然不够方便，后续需定义测试宏以满足调试需要。
        if(_quitFlag)
        {
            return;
        }
        cv::Mat img;
        _imageBuffer.pop(img);
        //cv::imshow("img",img);
        if(img.empty())
        {
            continue;
        }
        _aimArmor = _detector.detect(img);
        static uint8_t seq = 0;
        ControlFrame controlFrame;
        //Todo: 在未发现目标时发送的数据仍待商议
        if(_aimArmor.armorImg.empty())
        {
            controlFrame.frameSeq = seq++;
            controlFrame.model = false;
            controlFrame.yaw.angle = 0;
            controlFrame.pitch.angle = 0;
            _serial.send(controlFrame);
            continue;
        }
        float yaw,pitch;
        _soloveAngler.getAngle(_aimArmor.armorRotatedRect.center.x, _aimArmor.armorRotatedRect.center.y, yaw, pitch);
        Log_Info << "yaw : " << yaw;
        Log_Info << "pitch : " << pitch;
        controlFrame.frameSeq =  seq++;
        controlFrame.model = true;
        controlFrame.yaw.angle = yaw;
        controlFrame.pitch.angle = pitch;
        _serial.send(controlFrame);

    }
}

/*--------------------------------------------------------------------
\@brief         the program quit signal handler
*------------------------------------------------------------------*/
void AutomaticAimingSystem::signalHandler(int)
{
    _quitFlag = true;
}

/*--------------------------------------------------------------------
\@brief         init the program quit signal
*------------------------------------------------------------------*/
void AutomaticAimingSystem::initSignal()
{
    _quitFlag = false;
    struct sigaction sigact;
    sigact.sa_handler = signalHandler;
    sigemptyset(&sigact.sa_mask); // init signal set to empty
    sigact.sa_flags = 0;  //default behavior
    sigaction(SIGINT, &sigact, (struct sigaction*)NULL); //SIGINT(ctrl+c)
}

/*--------------------------------------------------------------------
\@brief         receive the serial message
*------------------------------------------------------------------*/
void AutomaticAimingSystem::serialReceiver()
{
    while(!_serialOpenFlag);
    while(true)
    {

    }
}